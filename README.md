# Satz-Reduktion

### Beschreibung
Reduktion / Vereinfachung von Sätze, z. B.: Ersetzung von
* Zeitangaben durch irgendwann
* Ortsangaben durch irgendwo (/ da / dort)
* Personen etc. durch irgendwer (/ jemand)
* irgendwem, irgendwas, ...

### Aufgaben
1. Definieren sinnvoller Ersetzungsschritte, -regeln und Abbruchkriterien
2. Erkennung typischer Ersetzungsmuster
3. Ansätze zur Vereinfachung von Teilsätzen, Relativsätze?
4. Evaluierung der Reduktion (Genauigkeit, Schwachstellen, Lösungsansätze)

### Betreuer
Erik Körner, koerner@informatik.uni-leipzig.de (Raum P906)

### Hinweise
* Anforderungen laut: http://asv.informatik.uni-leipzig.de/de/courses/257 
* Projekt-/Zeitplan (Gruppenmitglieder, Emails, Matrikelnr., Aufgaben, Aufgabenaufteilung, vorläufige Zeitplan, mögliche Zwischenergebnisse/-ziele)
* Sauberer Code, Ziel: Verständlichkeit für andere
* Accounts werden nach Anfrage erstellt, Details in extra Email

### Links

#### Stanford NER-Tool
* https://nlp.stanford.edu/software/CRF-NER.shtml
* https://mareikeschumacher.de/tutorial-wie-ich-stanford-ner-mit-deutschen-classifiern-installiere-und-nutze/ 
* https://nlpado.de/~sebastian/software/ner_german.shtml 

#### GermaNER
* https://github.com/tudarmstadt-lt/GermaNER 
* https://github.com/MaviccPRP/ger_ner_evals 
* 
